# syntax = docker/dockerfile:1.0-experimental
FROM rclone/rclone:latest
RUN --mount=type=secret,id=custom,dst=/data/custom.conf
COPY sync_path /
COPY entrypoint.sh /
ENTRYPOINT /entrypoint.sh