docker build . -t promed_db:latest
image=$(docker images -q promed_db:latest)
docker run -v uploads:/home/uploads/ -v orient-output:/home/results/orient_outputs/ -v simulation-output:/home/results/simulator_outputs/ --name promed_db $image
container=$(docker ps -aqf "name=promed_db")
docker rm $container
docker rmi $image